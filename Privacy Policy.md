# Privacy Policy #

### Information Collection and Use ###

For a better experience, while using our Service, we may require you to provide us with certain personally identifiable information. The information that we request will be retained on your device and is not collected by us in any way.

The app does use third party services that may collect information used to identify you.

Link to privacy policy of third party service providers used by the app

* [Firebase Services](https://firebase.google.com/support/privacy)

### Links to Other Sites ###

This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.

### Contact Us ###

If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us.
